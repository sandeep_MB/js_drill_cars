
let onlyCarYear = require('./problem4.js');

function carsBefore2000(inventory) {
    let array = onlyCarYear(inventory);
    let arr = [];
    let k = 0;
    for (let index = 0; index < array.length; index++) {
        if (array[index] < 2000) {
            arr.push(array[index]);
        }
    }
    return arr;
}

module.exports = carsBefore2000;