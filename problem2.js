function findLastCar(inventory) {

    let last = inventory.length - 1;
    let make = inventory[last].car_make;
    let model = inventory[last].car_model;

    return "Last car is a " + make + " " + model;
}

module.exports = findLastCar;
