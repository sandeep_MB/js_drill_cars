function sortByAlphabet(inventory) {
    let array = [...inventory];
    for (let index = 0; index < array.length; index++) {
        for (let index2 = 0; index2 < array.length - index - 1; index2++) {
            if (array[index2].car_model.toLowerCase() > array[index2 + 1].car_model.toLowerCase()) {
                let temp = array[index2];
                array[index2] = array[index2 + 1];
                array[index2 + 1] = temp;
            }
        }
    }
    let array2 = [];
    for (let index = 0; index < array.length; index++) {
        array2.push(array[index].car_model);
    }
    return array2;
}

module.exports = sortByAlphabet;