function onlyCarYear(inventory) {
    let arr = [];
    for (let index1 = 0; index1 < inventory.length; index1++) {
        arr.push(inventory[index1].car_year);
    }
    return arr;
}

module.exports = onlyCarYear;
