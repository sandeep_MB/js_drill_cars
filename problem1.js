function findCarById(inventory, id) {

    let year = inventory[id - 1].car_year;
    let make = inventory[id - 1].car_make;
    let model = inventory[id - 1].car_model;

    return "Car " + id + " is a " + year + " " + make + " " + model;
}

module.exports = findCarById;

