let inventory = require('../cars.js');

let sortByAlphabet = require('../problem3.js');

let actualResult = sortByAlphabet(inventory);

let expectedResult = ['300M', '4000CS Quattro', '525', '6 Series', 'Accord', 'Aerio',
    'Bravada', 'Camry', 'Cavalier', 'Ciera', 'Defender Ice Edition',
    'E-Class', 'Econoline E250', 'Escalade', 'Escort', 'Esprit', 'Evora',
    'Express 1500', 'Familia', 'Fortwo', 'G35', 'Galant', 'GTO', 'Intrepid',
    'Jetta', 'LSS', 'Magnum', 'Miata MX-5', 'Montero Sport', 'MR2', 'Mustang',
    'Navigator', 'Prizm', 'Q', 'Q7', 'R-Class', 'Ram Van 1500', 'Ram Van 3500',
    'riolet', 'Sebring', 'Skylark', 'Talon', 'Topaz', 'Town Car', 'TT', 'Windstar',
    'Wrangler', 'Wrangler', 'XC70', 'Yukon']

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
} else {
    console.log("Actual result is different from expected result");
}