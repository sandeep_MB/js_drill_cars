let inventory = require('../cars.js');

let findLastCar = require('../problem2.js');

let actualResult = findLastCar(inventory);
let expectedResult = 'Last car is a Lincoln Town Car';

if (actualResult === expectedResult) {
    console.log(actualResult);
} else {
    console.log("Actual result is different from expected result")
}

