let inventory = require('../cars.js');
let findCarById = require('../problem1.js');

let carId = 33;
let expectedResult = "Car 33 is a 2011 Jeep Wrangler";
let actualResult = findCarById(inventory, carId);

if (expectedResult === actualResult) {
    console.log(actualResult);
} else {
    console.log("Results are different");
}
