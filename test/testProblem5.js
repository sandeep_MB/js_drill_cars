let inventory = require('../cars.js');
let before2000 = require('../problem5.js');
let actualResult = before2000(inventory);

let expectedResult = [1983, 1990, 1995, 1987, 1996, 1997, 1999, 1987, 1995, 1994, 1985, 1997, 1992, 1993, 1964, 1999, 1991, 1997, 1992, 1998, 1965, 1996, 1995, 1996, 1999]

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
    console.log("Number of older cars: " + actualResult.length);
} else {
    console.log("Actual result is different from expected result");
}
