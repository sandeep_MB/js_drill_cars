function onlyAudiAndBMW(inventory) {
    let array = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === "Audi" || inventory[index].car_make === "BMW") {
            array.push(inventory[index]);
        }
    }
    return array;
}

module.exports = onlyAudiAndBMW;